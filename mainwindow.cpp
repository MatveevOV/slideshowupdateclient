#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

   m_pTcpSocket = new QTcpSocket(this);

   connect(m_pTcpSocket, &QTcpSocket::connected,this, &MainWindow::slotConnected);
   connect(m_pTcpSocket, &QTcpSocket::readyRead, this, &MainWindow::slotReadyRead);
   connect(m_pTcpSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(slotError(QAbstractSocket::SocketError)));
   connect(ui->settingsButton, &QPushButton::clicked, this, &MainWindow::showSettings);
   connect(ui->updateSoftButton, &QPushButton::clicked, this, &MainWindow::updateSoft);
   connect(ui->updateDataButton, &QPushButton::clicked, this, &MainWindow::updateData);

   QSettings connectionSettings (QString ("settings"), QSettings::IniFormat);

   serverIp = connectionSettings.value("ServerAddress", "127.0.0.1").toString();
   serverPort = connectionSettings.value("ServerPort", 54256).toInt();
   proxyIp = connectionSettings.value("ProxyAddress", "127.0.0.1").toString();
   proxyLogin = connectionSettings.value("ProxyLogin").toString();
   proxyPass = connectionSettings.value("ProxyPass" ).toString();
   proxyPort = connectionSettings.value("ProxyPort", 0).toInt();
   needProxy = connectionSettings.value("NeedProxy", false).toBool();

   qDebug() << needProxy;

   if (needProxy)
   {
       QNetworkProxy proxy;
       proxy.setType(QNetworkProxy::HttpProxy);
       proxy.setHostName(proxyIp);
       proxy.setPort(proxyPort);
       proxy.setUser(proxyLogin);
       proxy.setPassword(proxyPass);
       QNetworkProxy::setApplicationProxy(proxy);
   }


   m_pTcpSocket->connectToHost(serverIp,serverPort);


    m_nNextBlockSize = 0;


   QSettings settings (QString ("versions"), QSettings::IniFormat);


   currentSoftVersion = settings.value("SoftVersion", 1).value<quint16>();
   currentDataVersion = settings.value("DataVersion", 1).value<quint16>();

   ui->softVersionLabel->setText(QString("Версия приложения: %1").arg(currentSoftVersion));
   ui->dataVersionLabel->setText(QString("Версия слайдов: %1").arg(currentDataVersion));
   ui->progressBar->hide();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::slotReadyRead()
{
    QDataStream in(m_pTcpSocket);
    qDebug() << "client ready read" << m_nNextBlockSize <<m_pTcpSocket->bytesAvailable();
    in.setVersion(QDataStream::Qt_5_5);
    for (;;)
    {
        qDebug() << m_nNextBlockSize <<m_pTcpSocket->bytesAvailable();
        if (m_nNextBlockSize == 0) {
            if (m_pTcpSocket->bytesAvailable() < sizeof(quint64)) {
                break;
            }
            in >> m_nNextBlockSize;
        }

        if (m_pTcpSocket->bytesAvailable() < m_nNextBlockSize) {
            break;
        }

        quint8 type;
        in >> type;
        qDebug() << "type" << type;



        switch (type) {
        case RequestType::SoftVersion:
        {
            quint16 version;
            in >> version;
            qDebug() << "soft version" << version;
            newSoftVersion = version;
            if (version > currentSoftVersion)
                sendRequest(RequestType::Soft);
            else
            {
                QMessageBox msgBox;
                msgBox.setText("Обновление приложения не требуется");
                msgBox.setWindowTitle("Помощник по обновлению");
                msgBox.exec();
            }
            return;
        }
            break;
        case RequestType::Soft:
        {
            qDebug() << "soft receive";
            QFile file("OphtalmologicExamination.zip");
            receiveFile(file);
            return;

        }

            break;
        case RequestType::DataVersion:
        {
            quint16 version;
            in >> version;
            qDebug() << "data version" << version;
            newDataVersion = version;
            if (version > currentDataVersion)
                sendRequest(RequestType::Data);
            else
            {
                QMessageBox msgBox;
                msgBox.setText("Обновление слайдов не требуется");
                msgBox.setWindowTitle("Помощник по обновлению");
                msgBox.exec();
            }
            return;

        }
            break;
        case RequestType::Data:
        {
            qDebug() << "data receive";
            QFile file("data.zip");
            receiveFile(file);
            return;

        }

            break;
        default:
            break;
        }


        m_nNextBlockSize = 0;
    }

}

void MainWindow::slotError(QAbstractSocket::SocketError err)
{
    QString strError =(err == QAbstractSocket::HostNotFoundError ?
                        "Сервер недоступен" :
                        err == QAbstractSocket::RemoteHostClosedError ?
                        "Сервер закрыл соединение" :
                        err == QAbstractSocket::ConnectionRefusedError ?
                        "Соединение разорвано" :
                        QString(m_pTcpSocket->errorString())
                       );
       ui->statusbar->showMessage(strError);
       m_pTcpSocket->close();
       QTimer::singleShot(5000, this, &MainWindow::reconnectToServer);
}

void MainWindow::sendRequest(MainWindow::RequestType request)
{
    if (m_pTcpSocket->state() == QAbstractSocket::ConnectedState)
    {
        QByteArray block;
        QDataStream out(&block, QIODevice::WriteOnly);
        out.setVersion(QDataStream::Qt_5_5);
        out << quint64(0) << quint8(request);
        out.device()->seek(0);
        out << quint64(block.size() - sizeof(quint64));
        m_pTcpSocket->write(block);
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("Нет соединения с сервером");
        msgBox.setWindowTitle("Помощник по обновлению");
        msgBox.exec();
    }

}

void MainWindow::slotConnected()
{
    ui->statusbar->showMessage("Соединение с сервером установлено");


}

void MainWindow::reconnectToServer()
{
    qDebug() << m_pTcpSocket->state();
    if (m_pTcpSocket->state() == QAbstractSocket::UnconnectedState)
    {
        qDebug() << "reconnect";
        m_pTcpSocket->connectToHost(serverIp,serverPort);

    }
}

void MainWindow::receiveFile(QFile &file)
{
    ui->progressBar->show();
    this->setEnabled(false);
    quint64 fileSize;
    quint32 fileNameLength;
    QString fileName;

    QByteArray bArray;
    QDataStream dStream(&bArray, QIODevice::ReadWrite);
    dStream.setVersion(QDataStream::Qt_5_5);
    bArray = m_pTcpSocket->readAll();

    dStream >> fileSize;

    dStream >> fileNameLength;

    fileName = bArray.mid(sizeof(quint64) + sizeof(quint32), fileNameLength);


    file.open(QFile::WriteOnly);
    QDataStream write(&file);
    write.setVersion(QDataStream::Qt_5_5);

    write.writeRawData(bArray.data(), bArray.size());

    quint64 lBytesDone = 0;
    quint64 lSize;
    if (fileSize < bArray.size())
        lSize = 0;
    else
        lSize = fileSize - bArray.size();

    quint64 lBytes = 0;

    qDebug() << "fileInfo" << fileSize << lSize << fileName << bArray.size();
    ui->progressBar->setMaximum(lSize);
    while (lBytesDone < lSize)
    {
        lBytes = 0;
        while (lBytes == 0) lBytes = m_pTcpSocket->waitForReadyRead(-1);
        QByteArray tmp = m_pTcpSocket->readAll();
        lBytes += write.writeRawData(tmp.data(), tmp.size());
        lBytesDone += tmp.size();
        ui->progressBar->setValue(lBytesDone);

    }

    file.close();

    if (fileSize != 0)
    {
        QSettings settings (QString ("versions"), QSettings::IniFormat);
        if(file.fileName() == "data.zip")
        {
            currentDataVersion = newDataVersion;
            ui->dataVersionLabel->setText(QString("Версия слайдов: %1").arg(currentDataVersion));
            settings.setValue("DataVersion", currentDataVersion);
            QDir dir("./data");
            dir.removeRecursively();
        }
        if(file.fileName() == "OphtalmologicExamination.zip")
        {
            currentSoftVersion = newSoftVersion;
            ui->softVersionLabel->setText(QString("Версия приложения: %1").arg(currentSoftVersion));
            settings.setValue("SoftVersion", currentSoftVersion);
        }
        unzipArchive(file.fileName());

    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("Не удалось загрузить обновление");
        msgBox.setWindowTitle("Помощник по обновлению");
        msgBox.exec();
        QFile::remove(file.fileName());
    }

    ui->progressBar->hide();
    this->setEnabled(true);


}

void MainWindow::showSettings()
{
    Settings *settingsDialog = new Settings(serverIp, serverPort, needProxy, proxyIp, proxyPort, proxyLogin, proxyPass);
    settingsDialog->show();
    connect(settingsDialog, &Settings::settingsChanged, this, &MainWindow::changeSettings);

}

void MainWindow::updateSoft()
{
    sendRequest(RequestType::SoftVersion);
    m_nNextBlockSize = 0;
}

void MainWindow::updateData()
{
    sendRequest(RequestType::DataVersion);
    m_nNextBlockSize = 0;

}

void MainWindow::changeSettings(QString serverIp,
                                int serverPort, bool needProxy, QString proxyIp, int proxyPort, QString proxyLogin, QString proxyPass)
{
    this->serverIp = serverIp;
    this->serverPort = serverPort;
    this->proxyIp = proxyIp;
    this->proxyLogin = proxyLogin;
    this->proxyPass = proxyPass;
    this->proxyPort = proxyPort;
    this->needProxy = needProxy;

    QSettings connectionSettings (QString ("settings"), QSettings::IniFormat);
    connectionSettings.setValue("ServerAddress", this->serverIp);
    connectionSettings.setValue("ServerPort", this->serverPort);
    connectionSettings.setValue("ProxyAddress", this->proxyIp);
    connectionSettings.setValue("ProxyLogin", this->proxyLogin);
    connectionSettings.setValue("ProxyPass", this->proxyPass);
    connectionSettings.setValue("ProxyPort", this->proxyPort);
    connectionSettings.setValue("NeedProxy", this->needProxy);

    if (this->needProxy)
    {
        QNetworkProxy proxy;
        proxy.setType(QNetworkProxy::HttpProxy);
        proxy.setHostName(this->proxyIp);
        proxy.setPort(this->proxyPort);
        proxy.setUser(this->proxyLogin);
        proxy.setPassword(this->proxyPass);
        QNetworkProxy::setApplicationProxy(proxy);
    }


    m_pTcpSocket->disconnectFromHost();
    m_pTcpSocket->connectToHost(this->serverIp, this->serverPort);


}

void MainWindow::closeEvent(QEvent *event)
{
    m_pTcpSocket->disconnectFromHost();
    event->accept();
}

void MainWindow::unzipArchive(QString zipName)
{

    QDir extractDir(".");
    QuaZip zip(zipName);
    if (zip.open(QuaZip::mdUnzip))
    {
        zip.setFileNameCodec("IBM 866");
        qDebug() << "Opened";
        QuaZipFile zipFile(&zip);
        for (bool more = zip.goToFirstFile(); more; more = zip.goToNextFile()) {
            zipFile.open(QIODevice::ReadOnly);
            QByteArray ba = zipFile.readAll();
            zipFile.close();

            QString text = zip.getCurrentFileName();

            //cодержимое архива нужно поместить в папку extractDir
            QString newFilePath = QString(extractDir.dirName() + "/" + text);
            QFileInfo fi(newFilePath);
            QDir dir;
            dir.mkdir(fi.absoluteDir().path());
            // set destination file
            QFile dstFile(newFilePath);
            // open the destination file
            dstFile.open(QIODevice::WriteOnly);
            // write the data from the bytes array into the destination file
            dstFile.write(ba);
            //close the destination file
            dstFile.close();

            //qDebug() << zip.getCurrentFileName();
        }
        if (zip.getZipError() == UNZ_OK) {
            // ok, there was no error
        }

   zip.close();



    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("Не удалось распаковать полученный файл");
        msgBox.setWindowTitle("Помощник по обновлению");
        msgBox.exec();


    }


    QFile::remove(zipName);




}

