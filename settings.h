#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>

namespace Ui {
class Settings;
}

class Settings : public QDialog
{
    Q_OBJECT


public:
    explicit Settings(QString serverIp, int serverPort,
                      bool needProxy, QString proxyIp, int proxyPort, QString proxyLogin, QString proxyPass, QWidget *parent = 0);
    ~Settings();

private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();



    void on_needProxyCheckBox_toggled(bool checked);

private:
    Ui::Settings *ui;


signals:
    void settingsChanged(QString serverIp, int serverPort,
                         bool needProxy, QString proxyIp, int proxyPort, QString proxyLogin, QString proxyPass);
};

#endif // SETTINGS_H
