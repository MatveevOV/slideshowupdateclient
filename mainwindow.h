#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include <QFileDialog>
#include <QFile>
#include <QDir>
#include <QMessageBox>
#include <QEvent>
#include <QDialog>
#include <QSettings>
#include <QUuid>
#include <QNetworkProxy>
#include <QTimer>

#include "settings.h"
#include "quazip/quazip.h"
#include "quazip/quazipfile.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    enum RequestType {
        SoftVersion,
        Soft,
        DataVersion,
        Data
    };


private:
    Ui::MainWindow *ui;

    QTcpSocket* m_pTcpSocket;
    quint64     m_nNextBlockSize;
    int serverPort;
    QString serverIp;
    bool needProxy;
    int proxyPort;
    QString proxyIp;
    QString proxyLogin;
    QString proxyPass;



    quint16 currentSoftVersion;
    quint16 currentDataVersion;

    quint16 newSoftVersion;
    quint16 newDataVersion;

    QTimer timer;

private slots:
    void slotReadyRead   ();
    void slotError       (QAbstractSocket::SocketError err);
    void slotConnected();
    void reconnectToServer();
    void sendRequest(RequestType request);

    void receiveFile(QFile &file);

    void showSettings();
    void updateSoft();
    void updateData();

    void changeSettings(QString serverIp, int serverPort,
                        bool needProxy, QString proxyIp, int proxyPort, QString proxyLogin, QString proxyPass);

    void closeEvent(QEvent *event);

    void unzipArchive(QString zipName);

};

#endif // MAINWINDOW_H
