#include "settings.h"
#include "ui_settings.h"

Settings::Settings(QString serverIp, int serverPort,
                   bool needProxy, QString proxyIp, int proxyPort, QString proxyLogin, QString proxyPass, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Settings)
{
    ui->setupUi(this);
    ui->ipEdit->setText(serverIp);
    ui->portEdit->setRange(0,65535);
    ui->portEdit->setValue(serverPort);
    ui->needProxyCheckBox->setChecked(needProxy);
    ui->proxyAddressEdit->setText(proxyIp);
    ui->proxyPortEdit->setRange(0,65535);
    ui->proxyPortEdit->setValue(proxyPort);

    ui->proxyLogin->setText(proxyLogin);
    ui->proxyPass->setText(proxyPass);

    ui->needProxyCheckBox->setChecked(needProxy);



    ui->proxyAddressEdit->setEnabled(needProxy);
    ui->proxyPortEdit->setEnabled(needProxy);
    ui->proxyLogin->setEnabled(needProxy);
    ui->proxyPass->setEnabled(needProxy);
}

Settings::~Settings()
{
    delete ui;
}

void Settings::on_buttonBox_accepted()
{

    bool needProxy = false;
    if (ui->needProxyCheckBox->checkState() == Qt::Checked)
        needProxy = true;
    emit settingsChanged(ui->ipEdit->text(), ui->portEdit->value(),
                         needProxy, ui->proxyAddressEdit->text(), ui->proxyPortEdit->value(), ui->proxyLogin->text(), ui->proxyPass->text());
}

void Settings::on_buttonBox_rejected()
{
    close();
}




void Settings::on_needProxyCheckBox_toggled(bool checked)
{

        ui->proxyAddressEdit->setEnabled(checked);
        ui->proxyPortEdit->setEnabled(checked);
        ui->proxyLogin->setEnabled(checked);
        ui->proxyPass->setEnabled(checked);


}
